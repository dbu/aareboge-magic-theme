<?php

function aareboge_render_block_latest_posts($attributes) {
    global $GLOBALS; // :puke: the post we want to render into globals

    $args = array(
        'numberposts' => $attributes['postsToShow'],
        'post_status' => 'publish',
        'order'       => $attributes['order'],
        'orderby'     => $attributes['orderBy'],
    );

    if ( isset( $attributes['categories'] ) ) {
        $args['category'] = $attributes['categories'];
    }

    $recent_posts = wp_get_recent_posts( $args );

    $list_items_markup = '<div class="article-teasers">';

    foreach ( $recent_posts as $post ) {
        $GLOBALS['post'] = $post['ID'];
        ob_start();
        get_template_part('template-parts/content');
        $list_items_markup .= ob_get_clean();
    }

    return $list_items_markup.'</div>';
}

function aareboge_overwrite_block_latest_posts() {
    $blockType = WP_Block_Type_Registry::get_instance()->get_registered('core/latest-posts');
    if (!$blockType) return; // should not happen

    $blockType->render_callback = 'aareboge_render_block_latest_posts';
}

add_action( 'init', 'aareboge_overwrite_block_latest_posts');
