<?php
/**
 * The template for displaying autor archive pages
 */

use Molongui\Authorship\Includes\Author;

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main author-page" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
				?>
                <div class="author-info">
                    <div class="author-avatar">
                        <?= get_avatar( get_the_author_meta( 'user_email' ), 128 ); ?>
                    </div><!-- .author-avatar -->

                    <div class="author-description">
                        <p class="author-bio">
                            <?php the_author_meta( 'description' ); ?>
                        </p><!-- .author-bio -->
                    </div><!-- .author-description -->
                    <?php
                        $molonguiAuthor = new Author();
                        $author = $molonguiAuthor->get_data(get_the_author_ID(), 'notguest');
                        $plugin = false;molongui_get_plugin( MOLONGUI_AUTHORSHIP_ID, $plugin );
                        $settings = $plugin->settings;
                        require(MOLONGUI_AUTHORSHIP_DIR . 'public/views/parts/html-author-box-socialmedia.php');
                    ?>
                </div><!-- .author-info -->

            </header><!-- .page-header -->

            <div class="article-teasers">
			<?php
			// Start the Loop.
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );

				// End the loop.
			endwhile;
			?>
            </div>

            <?php
			// Previous/next page navigation.
			the_posts_pagination(
				array(
					'prev_text'          => __( 'Previous page', 'twentysixteen' ),
					'next_text'          => __( 'Next page', 'twentysixteen' ),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
				)
			);

			// If no content, include the "No posts found" template.
		else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
