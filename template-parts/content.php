<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
			<span class="sticky-post"><?php _e( 'Featured', 'twentysixteen' ); ?></span>
		<?php endif; ?>

		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

        <time datetime="<?= esc_attr( get_the_date( 'c')) ?>" class="wp-block-latest-posts__post-date">
            <?= esc_html( get_the_date( '')) ?>
        </time>

    </header><!-- .entry-header -->

    <?php twentysixteen_post_thumbnail(true); ?>

    <?php twentysixteen_excerpt(); ?>

    <div class="entry-summary">
        <a href="<?= esc_url( get_permalink()) ?>">Weiterlesen...</a>
    </div>

</article><!-- #post-## -->
